﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class ObligationElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Obligation = "Obligation";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string ObligationId = "ObligationId";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string FulfillOn = "FulfillOn";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string AttributeAssignment = "AttributeAssignment";
            }
        }
    }
}