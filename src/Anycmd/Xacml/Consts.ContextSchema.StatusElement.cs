﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class ContextSchema
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class StatusElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Status = "Status";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string StatusCode = "StatusCode";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string StatusMessage = "StatusMessage";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string StatusDetail = "StatusDetail";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Value = "Value";
            }
        }
    }
}