﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class ContextSchema
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class RequestElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Request = "Request";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Subject = "Subject";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Resource = "Resource";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Action = "Action";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Environment = "Environment";
            }
        }
    }
}