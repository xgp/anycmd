﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class ObligationsElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Obligations = "Obligations";
            }
        }
    }
}