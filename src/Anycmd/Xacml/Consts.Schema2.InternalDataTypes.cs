﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema2
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public class InternalDataTypes : Schema1.InternalDataTypes
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string IPAddress = "urn:oasis:names:tc:xacml:2.0:data-type:ipAddress";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string DnsName = "urn:oasis:names:tc:xacml:2.0:data-type:dnsName";
            }
        }
    }
}