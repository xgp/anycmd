﻿
namespace Anycmd.Engine.Edi.Messages
{
    using Events;
    using Abstractions;

    public class ElementInfoRuleUpdatedEvent : DomainEvent
    {
        public ElementInfoRuleUpdatedEvent(IUserSession userSession, ElementInfoRule source) : base(userSession, source) { }
    }
}
