﻿
namespace Anycmd.Engine.Ac.Abstractions.Infra
{
    /// <summary>
    /// 表示IO方向的值。
    /// </summary>
    public enum IoDirection
    {
        Input,
        Output
    }
}
