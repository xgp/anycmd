﻿
namespace Anycmd.Ac.ViewModels.Infra.ResourceViewModels
{
    using Engine;
    using Engine.Ac.InOuts;
    using Engine.Ac.Messages.Infra;
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceTypeCreateInput : EntityCreateInput, IResourceTypeCreateIo
    {
        public ResourceTypeCreateInput()
        {
            HecpOntology = "ResourceType";
            HecpVerb = "Create";
        }

        public Guid AppSystemId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int SortCode { get; set; }

        public override IAnycmdCommand ToCommand(IUserSession userSession)
        {
            return new AddResourceCommand(userSession, this);
        }
    }
}
