﻿
namespace Anycmd.Ac.ViewModels.Infra.UIViewViewModels
{
    using Engine;
    using Engine.Ac.InOuts;
    using Engine.Ac.Messages.Infra;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class UiViewButtonUpdateInput : IUiViewButtonUpdateIo
    {
        public UiViewButtonUpdateInput()
        {
            HecpOntology = "UiViewButton";
            HecpVerb = "Update";
        }

        public string HecpOntology { get; private set; }

        public string HecpVerb { get; private set; }

        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid? FunctionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsEnabled { get; set; }

        public IAnycmdCommand ToCommand(IUserSession userSession)
        {
            return new UpdateUiViewButtonCommand(userSession, this);
        }
    }
}
